package com.example.demo.service;

import com.example.demo.model.DataTransfer;
import com.example.demo.repository.DataTransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * @author tomask79
 */
@Service
public class DataTransferService implements IDataTransferService {

    private DataTransferRepository dataTransferRepository;

    @Autowired
    public void setDataTransferRepository(DataTransferRepository dataTransferRepository) {
        this.dataTransferRepository = dataTransferRepository;
    }


    public DataTransfer save(DataTransfer book) {
        return dataTransferRepository.save(book);
    }

    public void delete(DataTransfer book) {
        dataTransferRepository.delete(book);
    }

    @Override
    public DataTransfer findOne(String id) {
        return dataTransferRepository.findById(id).get();
    }

    @Override
    public Page<DataTransfer> findByDataExchangeCode(String dataExchangeCode, PageRequest pageRequest) {
        return dataTransferRepository.findByDataExchangeCode(dataExchangeCode, pageRequest);
    }
}

