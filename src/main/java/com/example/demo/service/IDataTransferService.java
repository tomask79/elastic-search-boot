package com.example.demo.service;

import com.example.demo.model.DataTransfer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * @author tomask79
 */
public interface IDataTransferService {

    DataTransfer save(DataTransfer book);

    void delete(DataTransfer book);

    DataTransfer findOne(String id);

    Page<DataTransfer> findByDataExchangeCode(String dataExchangeCode, PageRequest pageRequest);
}
