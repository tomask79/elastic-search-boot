package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "dataexchangecode", type = "dataTransferCode")
public class DataTransfer {

    @Id
    private String id;

    private String dataExchangeCode;

    private String data;

    public DataTransfer() {
    }

    /**
     * @param id
     * @param dataExchangeCode
     * @param data
     */
    public DataTransfer(final String id, final String dataExchangeCode, final String data) {
        this.id = id;
        this.dataExchangeCode = dataExchangeCode;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataExchangeCode() {
        return dataExchangeCode;
    }

    public void setDataExchangeCode(String dataExchangeCode) {
        this.dataExchangeCode = dataExchangeCode;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return this.id+
               this.dataExchangeCode+
               this.id;
    }
}