package com.example.demo.mvc;

import com.example.demo.model.DataTransfer;
import com.example.demo.repository.DataTransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class HomeController {

    @Autowired
    private DataTransferRepository dataTransferRepository;

    @GetMapping(value = "/prepareData")
    public String prepareESData() {
        final DataTransfer data = new DataTransfer("1", "AW_INPUT", "<XML>");
        dataTransferRepository.save(data);

        final DataTransfer data2 = new DataTransfer("2", "BSL_INPUT", "<XML>");
        dataTransferRepository.save(data2);

        return "Data saved into elastic search!";
    }

    @GetMapping(value = "/get/{id}")
    public DataTransfer getDataTransfer(@PathVariable("id") String id) {
        return dataTransferRepository.findById(id).get();
    }
}
