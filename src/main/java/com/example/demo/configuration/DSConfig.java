package com.example.demo.configuration;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeValidationException;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.elasticsearch.client.TransportClientFactoryBean;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.io.File;
import java.net.InetAddress;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.example.demo.repository")
public class DSConfig {

    @Value("${elasticsearch.host}")
    private String EsHost;

    @Value("${elasticsearch.port}")
    private int EsPort;

    @Value("${elasticsearch.clustername}")
    private String EsClusterName;

    @Profile("!localNode")
    @Bean
    public Client client() throws Exception {
        /**
         * PreBuiltTransportClient works fine, but requires Netty3Plugin
         * and Spring Boot offers only Netty4Plugin. Needs extra dependencies.

         Settings settings = Settings.builder()
                          .put("cluster.name", EsClusterName).build();
         TransportClient client = new PreBuiltTransportClient(settings);
         client.addTransportAddress(
                new InetSocketTransportAddress(
                        InetAddress.getByName(EsHost), EsPort));
         return client;
         */

        TransportClientFactoryBean client = new TransportClientFactoryBean();
        client.setClusterName(EsClusterName);

        client.afterPropertiesSet();

        return client.getObject();
    }

    @Profile("!localNode")
    @Bean(name = "elasticsearchTemplate")
    public ElasticsearchOperations elasticsearchTemplate1() throws Exception {
        return new ElasticsearchTemplate(client());
    }

    @Profile("localNode")
    @Bean(name="elasticsearchTemplate")
    public ElasticsearchTemplate elasticsearchTemplate2() throws Exception {
        return new ElasticsearchTemplate(createLocalNode().client());
    }

    @Profile("localNode")
    @Bean
    public Node createLocalNode() throws NodeValidationException {
        final String tmpDir = System.getProperty("java.io.tmpdir");
        Settings.Builder elasticsearchSettings =
                Settings.builder()
                        .put("cluster.name", EsClusterName)
                        .put("path.data", new File(tmpDir, "data").getAbsolutePath())
                        .put("path.logs", new File(tmpDir, "logs").getAbsolutePath())
                        .put("transport.type","local")
                        .put("http.enabled","false")
                        .put("path.home", tmpDir);

        final Node node = new Node(elasticsearchSettings.build());
        node.start();

        return node;
    }
}

