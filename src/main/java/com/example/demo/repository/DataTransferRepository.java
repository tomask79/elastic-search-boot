package com.example.demo.repository;

import com.example.demo.model.DataTransfer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author tomask79
 */
public interface DataTransferRepository extends ElasticsearchRepository<DataTransfer, String> {

    Page<DataTransfer> findByDataExchangeCode(String dataExchangeCode, Pageable pageable);
}
