## Using the ElasticSearch in Spring Boot 2.0 ##

Well, I've been doing a lot of "DevOps" lately so let's dive into Spring and something uselfull again.
In our company's landing system we use database for audithing the external webservice or messaging calls. 
This isn't certainly a long term solution and in our team we're thinking about replacing this mechanism with [ElasticSearch](https://www.elastic.co)
So I wanted to check the current state of the ElasticSearch support in [Spring Boot](https://spring.io/projects/spring-boot)

## Goal of the demo ##

* Connecting to and querying the embedded ElasticSearch node with [Spring Data ElasticSearch](https://spring.io/projects/spring-data-elasticsearch)
* Connecting to and querying the external ElasticSearch node with [Spring Data ElasticSearch](https://spring.io/projects/spring-data-elasticsearch)
* Everything running with ElasticSearch API 5.5.0 embedded in Spring Boot 2.0

## Demo prerequisities ##

We're going to save following document into ElasticSearch:

    @Document(indexName = "dataexchangecode", type = "dataTransferCode")
    public class DataTransfer {

        @Id
        private String id;

        private String dataExchangeCode;

        private String data;

        public DataTransfer() {
        }

        /**
         * @param id
         * @param dataExchangeCode
         * @param data
         */
        public DataTransfer(final String id, final String dataExchangeCode, final String data) {
            this.id = id;
            this.dataExchangeCode = dataExchangeCode;
            this.data = data;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDataExchangeCode() {
            return dataExchangeCode;
        }

        public void setDataExchangeCode(String dataExchangeCode) {
            this.dataExchangeCode = dataExchangeCode;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return this.id+
               this.dataExchangeCode+
               this.id;
        }
    }
to get into the picture of what the indexName and type parameters means check the following [document](https://www.elastic.co/guide/en/elasticsearch/guide/current/index-doc.html). 
For the test purposes let's create following two REST controllers for invoking the Spring Data ElasticSearch CRUD repository:

    @Component
    @RestController
    public class HomeController {

        @Autowired
        private DataTransferRepository dataTransferRepository;

        @GetMapping(value = "/prepareData")
        public String prepareESData() {
            final DataTransfer data = new DataTransfer("1", "AW_INPUT", "<XML>");
            dataTransferRepository.save(data);

            final DataTransfer data2 = new DataTransfer("2", "BSL_INPUT", "<XML>");
            dataTransferRepository.save(data2);

            return "Data saved into elastic search!";
        }

        @GetMapping(value = "/get/{id}")
        public DataTransfer getDataTransfer(@PathVariable("id") String id) {
            return dataTransferRepository.findById(id).get();
        }
    }


## Connecting to and querying the embedded ElasticSearch node ##

When building local node bear in mind that ElasticSearch API 5.x has been changed regarding the NodeBuilder class designated for
building the local ES node. Check [this](https://stackoverflow.com/questions/35342083/nodebuilder-not-found-in-the-elasticsearch-api-application) stackoverflow discussion for example.
ES documentation states:

    NodeBuilder has been removed. While using Node directly within an application is not officially supported, 
    it can still be constructed with the Node(Settings) constructor.

which is exactly what I did:

    @Profile("localNode")
    @Bean
    public Node createLocalNode() throws NodeValidationException {
        final String tmpDir = System.getProperty("java.io.tmpdir");
        Settings.Builder elasticsearchSettings =
                Settings.builder()
                        .put("cluster.name", EsClusterName)
                        .put("path.data", new File(tmpDir, "data").getAbsolutePath())
                        .put("path.logs", new File(tmpDir, "logs").getAbsolutePath())
                        .put("transport.type","local")
                        .put("http.enabled","false")
                        .put("path.home", tmpDir);

        final Node node = new Node(elasticsearchSettings.build());
        node.start();

        return node;
    }

For the node parameters list check [following](https://www.elastic.co/guide/en/elasticsearch/reference/2.1/modules-http.html) documentation. 
To have the Spring Data ElasticSearch repository working we need ElasticsearchTemplate:

    @Profile("localNode")
    @Bean(name="elasticsearchTemplate")
    public ElasticsearchTemplate elasticsearchTemplate2() throws Exception {
        return new ElasticsearchTemplate(createLocalNode().client());
    }

This will spin-up an ElasticsearchTemplate with [NodeClient](https://www.elastic.co/guide/en/elasticsearch/client/java-api/2.0/node-client.html) for your needs. 
Definitelly be aware of the following two things:

* Difference between [NodeClient and TransportClient](https://www.elastic.co/guide/en/elasticsearch/guide/current/_transport_client_versus_node_client.html)
* And most off all that [TransportClient is deprecated](https://www.elastic.co/guide/en/elasticsearch/client/java-api/master/transport-client.html)

### Testing the local ElasticSearch node ###

At first of course we need to build the project:

    Macbooks-MacBook-Pro:demo tomask79$ mvn clean install

then run the Spring Boot 2.0 application with localNode spring profile:

    Macbooks-MacBook-Pro:demo tomask79$ java -jar -Dspring.profiles.active=localNode target/demo-0.0.1-SNAPSHOT.jar

After that ES local node should be started up:

    2018-10-14 22:08:12.549  INFO 469 --- [           main] o.e.n.Node                               : initializing ...
    2018-10-14 22:08:12.671  INFO 469 --- [           main] o.e.e.NodeEnvironment                    : using [1] data paths, mounts [[/ (/dev/disk1)]], net usable_space [365.6gb], net total_space [464.7gb], spins? [unknown], types [hfs]
    2018-10-14 22:08:12.675  INFO 469 --- [           main] o.e.e.NodeEnvironment                    : heap size [4gb], compressed ordinary object pointers [true]
    2018-10-14 22:08:12.678  INFO 469 --- [           main] o.e.n.Node                               : node name [3Ia4X6S] derived from node ID [3Ia4X6S1S9i4x4p8AhGbZw]; set [node.name] to override
    2018-10-14 22:08:12.678  INFO 469 --- [           main] o.e.n.Node                               : version[5.6.11-SNAPSHOT], pid[469], build[Unknown/Unknown], OS[Mac OS X/10.12.6/x86_64], JVM[Oracle Corporation/OpenJDK 64-Bit Server VM/11/11+28]
    2018-10-14 22:08:12.678  INFO 469 --- [           main] o.e.n.Node                               : JVM arguments [-Dspring.profiles.active=localNode]
    2018-10-14 22:08:12.678  WARN 469 --- [           main] o.e.n.Node                               : version [5.6.11-SNAPSHOT] is a pre-release version of Elasticsearch and is not suitable for production
    2018-10-14 22:08:12.684  INFO 469 --- [           main] o.e.p.PluginsService                     : no modules loaded
    2018-10-14 22:08:12.685  INFO 469 --- [           main] o.e.p.PluginsService                     : no plugins loaded
    2018-10-14 22:08:14.401  INFO 469 --- [           main] o.e.d.DiscoveryModule                    : using discovery type [zen]
    2018-10-14 22:08:14.874  INFO 469 --- [           main] o.e.n.Node                               : initialized
    2018-10-14 22:08:14.875  INFO 469 --- [           main] o.e.n.Node                               : starting ...
    2018-10-14 22:08:14.880  INFO 469 --- [           main] o.e.t.TransportService                   : publish_address {local[1]}, bound_addresses {local[1]}
    2018-10-14 22:08:17.938  INFO 469 --- [pdateTask][T#1]] o.e.c.s.ClusterService                   : new_master {3Ia4X6S}{3Ia4X6S1S9i4x4p8AhGbZw}{zOnd0hsHSNW7AJeKBj__xw}{local}{local[1]}, reason: zen-disco-elected-as-master ([0] nodes joined)[, ]
    2018-10-14 22:08:17.945  INFO 469 --- [           main] o.e.n.Node                               : started
    2018-10-14 22:08:17.983  INFO 469 --- [pdateTask][T#1]] o.e.g.GatewayService                     : recovered [0] indices into cluster_state
    2018-10-14 22:08:18.211  INFO 469 --- [pdateTask][T#1]] o.e.c.m.MetaDataCreateIndexService       : [dataexchangecode] creating index, cause [api], templates [], shards [5]/[1], mappings []
    2018-10-14 22:08:18.893  INFO 469 --- [pdateTask][T#1]] o.e.c.m.MetaDataMappingService           : [dataexchangecode/yvKR0D2vQWGGNC3OWwGbsA] create_mapping [dataTransferCode]

Now let's test it, first invoking of REST controller endpoint for loading the data into ElasticSearch:

    Macbooks-MacBook-Pro:demo tomask79$ curl http://localhost:8080/prepareData
    Data saved into elastic search!

Then we can query the data:

    Macbooks-MacBook-Pro:demo tomask79$ curl http://localhost:8080/get/1
    {"id":"1","dataExchangeCode":"AW_INPUT","data":"<XML>"}

    Macbooks-MacBook-Pro:demo tomask79$ curl http://localhost:8080/get/2
    {"id":"2","dataExchangeCode":"BSL_INPUT","data":"<XML>"}

Okay, local embedded node works fine!

## Connecting to and querying the external ElasticSearch node ##

In this case we don't need embedding any local ES node, we just need to start up ES client.
Following configuration is all over the internet:

        Settings settings = Settings.builder()
                          .put("cluster.name", EsClusterName).build();
         TransportClient client = new PreBuiltTransportClient(settings);
         client.addTransportAddress(
                new InetSocketTransportAddress(
                        InetAddress.getByName(EsHost), EsPort));
 
Well, ES API embedded inside of Spring Boot 2.0 doesn't work with **PreBuiltTransportClient**, because it requires 
Netty3Plugin which isn't wired up in the maven dependencies. If you still don't want to upgrade to [highlevel REST API](https://dzone.com/articles/java-high-level-rest-client-elasticsearch)
stick with **TransportClientFactoryBean** delegating to **SpringDataTransportClient**

    @Profile("!localNode")
    @Bean
    public Client client() throws Exception {
        /**
         * PreBuiltTransportClient works fine, but requires Netty3Plugin
         * and Spring Boot offers only Netty4Plugin. Needs extra dependencies.

         Settings settings = Settings.builder()
                          .put("cluster.name", EsClusterName).build();
         TransportClient client = new PreBuiltTransportClient(settings);
         client.addTransportAddress(
                new InetSocketTransportAddress(
                        InetAddress.getByName(EsHost), EsPort));
         return client;
         */

        TransportClientFactoryBean client = new TransportClientFactoryBean();
        client.setClusterName(EsClusterName);

        client.afterPropertiesSet();

        return client.getObject();
    }

and of course we need ElasticsearchTemplate again to have the Spring Data ES working:

    @Profile("!localNode")
    @Bean(name = "elasticsearchTemplate")
    public ElasticsearchOperations elasticsearchTemplate1() throws Exception {
        return new ElasticsearchTemplate(client());
    }

Code ready, let's test the whole scenario:

Open the terminal and start the external ES server

    Macbooks-MacBook-Pro:demo tomask79$ elasticsearch

Verify that external elastic search server is running

    Macbooks-MacBook-Pro:demo tomask79$ curl http://localhost:9200 
    {
      "name" : "NmF778a",
      "cluster_name" : "elasticsearch_tomask79",
      "cluster_uuid" : "Z0CfKNMxSNGqfkUONZ6bRg",
      "version" : {
        "number" : "6.4.2",
        "build_flavor" : "oss",
        "build_type" : "tar",
        "build_hash" : "04711c2",
        "build_date" : "2018-09-26T13:34:09.098244Z",
        "build_snapshot" : false,
        "lucene_version" : "7.4.0",
        "minimum_wire_compatibility_version" : "5.6.0",
        "minimum_index_compatibility_version" : "5.0.0"
      },
      "tagline" : "You Know, for Search"
    }

Now let's start the applicationn with default spring profile:

    Macbooks-MacBook-Pro:demo tomask79$ java -jar target/demo-0.0.1-SNAPSHOT.jar

Then let's repeat invoking of REST endpoints for loading the data into ES and querying:

    Macbooks-MacBook-Pro:demo tomask79$ curl http://localhost:8080/prepareData
    Data saved into elastic search!

    Macbooks-MacBook-Pro:demo tomask79$ curl http://localhost:8080/get/1
    {"id":"1","dataExchangeCode":"AW_INPUT","data":"<XML>"}

    Macbooks-MacBook-Pro:demo tomask79$ curl http://localhost:8080/get/2
    {"id":"2","dataExchangeCode":"BSL_INPUT","data":"<XML>"}
    
Connecting to external ES also works fine.

Best regards

Tomas
    
